#pragma once
#include "BitStream.h"
#include <string>
#include "MoveList.h"
#include "Move.h"

class ClientProxy
{
public:
	ClientProxy(RakNet::AddressOrGUID sysAddress, int playerId);
	~ClientProxy();

	int GetPlayerID() { return PlayerID; };

	MoveList& GetUnprocessedMoveList() { return mUnprocessedMoveList; };

	float ProcessPacket(RakNet::BitStream& bsIn);

	void SetIsLastMoveTimestampDirty(bool isdrty);

	bool IsLastMoveTimestampDirty() { return mIsLastMoveTimestampDirty; };

private:
	RakNet::AddressOrGUID SysAddress;
	int PlayerID;

	MoveList mUnprocessedMoveList;
	bool mIsLastMoveTimestampDirty;
};


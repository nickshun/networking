#include "stdafx.h"
#include "GameServer.h"
#include "NetworkServer.h"
#include "Paddle.h"
#include "Ball.h"
#include "SDL.h"

GameServer::GameServer()
{
	player1 = new Paddle(0, SCREEN_HEIGHT / 2);
	player1->setX(20 + player1->getWidth() / 2);
	player2 = new Paddle(0, SCREEN_HEIGHT / 2);
	player2->setX(SCREEN_WIDTH - 20 - player1->getWidth() / 2);
	ball = new Ball(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2);

	isRunning = false;
	waitingForPlayer = true;
	bindBallToPlayer = 0;
	score[0] = 0;
	score[1] = 0;
}

bool GameServer::Init()
{
	if (NetworkServer::Get().Init())
	{
		isRunning = true;
		return true;
	}
	else
	{
		return false;
	}
}

GameServer::~GameServer()
{
	delete ball;
	delete player2;
	delete player1;
}

void GameServer::wait()
{
	waitingForPlayer = true;
}

void GameServer::begin()
{
	resetMatch(0);
	waitingForPlayer = false;
}

Paddle* GameServer::GetPlayer(int i)
{
	if (i == 0)
	{
		return player1;
	}
	else if (i == 1)
	{
		return player2;
	}
	else
	{
		std::cout << "invalid player num: " << i << std::endl;
		return nullptr;
	}
}

void GameServer::Update()
{
	UpdateInput();

	if (!waitingForPlayer)
	{
		//Get ClientProxies for p1 and p2 and update paddle pos
		ClientProxy* proxy = NetworkServer::Get().GetProxy(0);

		if (proxy != nullptr)
		{
			player1->ProcessMoves(proxy->GetUnprocessedMoveList());
		}

		proxy = NetworkServer::Get().GetProxy(1);

		if (proxy != nullptr)
		{
			player2->ProcessMoves(proxy->GetUnprocessedMoveList());
		}

		if (gameDelay <= 0)
		{
			ball->Update();
			int isScore = ball->CheckCollision(player1, player2, SCREEN_WIDTH, SCREEN_HEIGHT);

			switch (isScore)
			{
			case -1:
				score[1]++;
				bindBallToPlayer = 1;
				resetMatch(isScore);
				gameDelay = 2000;
				break;
			case 0:
				gameDelay = 0;
				break;
			case 1:
				score[0]++;
				resetMatch(isScore);
				bindBallToPlayer = 0;
				gameDelay = 2000;
				break;
			}
		}
		else
		{
			bindBall();
			gameDelay -= FRAME_TIME;
		}
	}

	//NetworkUpdate
	NetworkServer::Get().Update();
}

void GameServer::bindBall()
{
	if (bindBallToPlayer == 0)
	{
		ball->setY(player1->getY());
	}
	else
	{
		ball->setY(player2->getY());
	}
}

void GameServer::UpdateInput()
{
	SDL_Event event;
	if (SDL_PollEvent(&event))
	{
		if (event.type == SDL_QUIT)
		{
			endGame();
		}

		if (event.type == SDL_KEYDOWN)
		{
			SDL_Keycode keyPressed = event.key.keysym.sym;

			switch (keyPressed)
			{
			case SDLK_ESCAPE:
				endGame();
				break;
			}
		}

		if (event.type == SDL_KEYUP)
		{
			SDL_Keycode keyPressed = event.key.keysym.sym;
		}
	}
}

void GameServer::endGame()
{
	isRunning = false;
}

void GameServer::resetMatch(int side)
{
	player1->setY(SCREEN_HEIGHT / 2);
	player2->setY(SCREEN_HEIGHT / 2);

	ball->setY(SCREEN_HEIGHT / 2);

	switch (side)
	{
	case -1:
		ball->setX(player2->getX() - player2->getWidth() / 2 - ball->getWidth() / 2);
		ball->setSpeedMod(1);
		break;
	case 1:
		ball->setX(player1->getX() + player1->getWidth() / 2 + ball->getWidth() / 2);
		ball->setSpeedMod(-1);
		break;
	}

	std::string scoreText = "Player 1: " + std::to_string(score[0]) + "Player 2: " + std::to_string(score[1]);

	std::cout << scoreText << std::endl;

	NetworkServer::Get().UpdateScore(scoreText);

	NetworkServer::Get().SendPlayer(0);
	NetworkServer::Get().SendPlayer(1);

	NetworkServer::Get().GetProxy(0)->GetUnprocessedMoveList().Clear();
	NetworkServer::Get().GetProxy(1)->GetUnprocessedMoveList().Clear();
}
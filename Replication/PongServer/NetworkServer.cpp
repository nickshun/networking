#include "stdafx.h"
#include "NetworkServer.h"
#include "GameServer.h"
#include <string>
#include"Paddle.h"
#include "Ball.h"

NetworkServer::NetworkServer():
	counter(SEND_DATA_COUNT)
{
}

NetworkServer::~NetworkServer()
{

}

bool NetworkServer::Init()
{
	peer = RakNet::RakPeerInterface::GetInstance();

	RunServer(peer);

	printf("My GUID is %s\n", peer->GetGuidFromSystemAddress(RakNet::UNASSIGNED_SYSTEM_ADDRESS).ToString());

	return true;
}

void NetworkServer::Update()
{
	counter--;
	RunCommon(peer);

	if (counter < 0)
	{
		counter = SEND_DATA_COUNT;

		RakNet::BitStream bsOut3;
		bsOut3.Write((RakNet::MessageID)ID_BALL_POSITION_MESSAGE);
		GameServer::Get().GetBall()->Write(&bsOut3);
		peer->Send(&bsOut3, HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
		SendPlayer(0);
		SendPlayer(1);
	}
}

unsigned char NetworkServer::GetPacketIdentifier(RakNet::Packet *p)
{
	if (p == 0)
		return 255;

	if ((unsigned char)p->data[0] == ID_TIMESTAMP)
	{
		RakAssert(p->length > sizeof(RakNet::MessageID) + sizeof(RakNet::Time));
		return (unsigned char)p->data[sizeof(RakNet::MessageID) + sizeof(RakNet::Time)];
	}
	else
		return (unsigned char)p->data[0];
}

void NetworkServer::SendStringToPeer(std::string aMessage, RakNet::MessageID messageID, RakNet::AddressOrGUID systemAddress, RakNet::RakPeerInterface* peer)
{
	// Use a BitStream to write a custom user message
	// Bitstreams are easier to use than sending casted structures, and handle endian swapping automatically
	RakNet::BitStream bsOut;
	bsOut.Write(messageID);
	bsOut.Write(aMessage.c_str());

	peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, systemAddress, false);

}

void NetworkServer::BroadcastStringFromSystemToAll(std::string aMessage, RakNet::MessageID messageID, RakNet::AddressOrGUID systemAddress, RakNet::RakPeerInterface* peer)
{
	RakNet::BitStream bsOut;
	bsOut.Write(messageID);
	bsOut.Write(aMessage.c_str());
	//UNASSIGNED_SYSTEM_ADDRESS means we dont ignore anyone on the list (no filters)
	//The "true" as the last parameter means we want to broadcast to all
	peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, systemAddress, true);

}

void NetworkServer::BroadcastStringToAll(std::string aMessage, RakNet::MessageID messageID, RakNet::RakPeerInterface* peer)
{
	BroadcastStringFromSystemToAll(aMessage, messageID, RakNet::UNASSIGNED_SYSTEM_ADDRESS, peer);
}

RakNet::RakString NetworkServer::GetStringFromPacket(RakNet::Packet* aPacket)
{
	RakNet::RakString aName;
	RakNet::BitStream bsIn(aPacket->data, aPacket->length, false);
	bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
	bsIn.Read(aName);
	return aName;

}

void NetworkServer::RunServer(RakNet::RakPeerInterface* server)
{
	server->SetIncomingPassword("Rumpelstiltskin", (int)strlen("Rumpelstiltskin"));
	//Concider us disconnected if we don't respond for 3 seconds
	server->SetTimeoutTime(30000, RakNet::UNASSIGNED_SYSTEM_ADDRESS);

	RakNet::SocketDescriptor socketDescriptor;
	socketDescriptor.port = 200;
	socketDescriptor.socketFamily = AF_INET; // Test out IPV4
	bool b = server->Startup(2, &socketDescriptor, 1) == RakNet::RAKNET_STARTED;
	server->SetMaximumIncomingConnections(2);

	//Make sure connection doesn't die out if we havent sent a message in a while
	server->SetOccasionalPing(true);
	//Set "time-out" to 1 sec, but this is only for unreliable messages
	server->SetUnreliableTimeout(1000);

	printf("\nMy IP addresses AS A SERVER:\n");
	for (unsigned int i = 0; i < server->GetNumberOfAddresses(); i++)
	{
		RakNet::SystemAddress sa = server->GetInternalID(RakNet::UNASSIGNED_SYSTEM_ADDRESS, i);
		printf("%i. %s (LAN=%i)\n", i + 1, sa.ToString(false), sa.IsLANAddress());
	}
}

void NetworkServer::RunCommon(RakNet::RakPeerInterface* peer)
{
	RakNet::Packet* aPacket = nullptr;

	for (aPacket = peer->Receive(); aPacket; peer->DeallocatePacket(aPacket), aPacket = peer->Receive())
	{
		// We got a packet, get the identifier with our handy function
		auto packetIdentifier = GetPacketIdentifier(aPacket);

		// Check if this is a network message packet
		switch (packetIdentifier)
		{
			// SERVER
		case ID_NEW_INCOMING_CONNECTION:
		{
			// Somebody connected.  We have their IP now
			printf("ID_NEW_INCOMING_CONNECTION from %s with GUID %s\n", aPacket->systemAddress.ToString(true), aPacket->guid.ToString());

			RakNet::BitStream bsOut2;
			bsOut2.Write((RakNet::MessageID)ID_WELCOME_MESSAGE_1);

			bool Player1Open = true;
			bool Player2Open = true;

			for (std::map<std::string, ClientProxy*>::iterator iter = AddressToProxy.begin(); iter != AddressToProxy.end(); ++iter)
			{
				switch (iter->second->GetPlayerID())
				{
				case 0:
					Player1Open = false;
					break;
				case 1:
					Player2Open = false;
					break;
				}
			}

			int PlayerNum = -1;

			if (Player1Open)
			{
				PlayerNum = 0;
				std::cout << "making player 1" << std::endl;
			}
			else if (Player2Open)
			{
				PlayerNum = 1;
				std::cout << "making player 2" << std::endl;
			}
			
			if (PlayerNum != -1)
			{
				PlayerNumToClient.insert(std::make_pair(PlayerNum, aPacket->systemAddress));
				ClientProxy* proxy = new ClientProxy(aPacket->systemAddress, PlayerNum);
				AddressToProxy.insert(std::make_pair(aPacket->systemAddress.ToString(), proxy));
				NumToProxy.insert(std::make_pair(PlayerNum, proxy));
				bsOut2.Write(PlayerNum);
				std::cout << "num player: " << PlayerNumToClient.size() << std::endl;

				peer->Send(&bsOut2, HIGH_PRIORITY, RELIABLE_ORDERED, 0, aPacket->systemAddress, false);
			}

		}
			break;
			// COMMON
		case ID_DISCONNECTION_NOTIFICATION:
			// Connection lost normally
			printf("ID_DISCONNECTION_NOTIFICATION\n");

			if (AddressToProxy.find(aPacket->systemAddress.ToString()) != AddressToProxy.end())
			{
				int playerNum = AddressToProxy[aPacket->systemAddress.ToString()]->GetPlayerID();

				delete AddressToProxy[aPacket->systemAddress.ToString()];
				AddressToProxy.erase(AddressToProxy.find(aPacket->systemAddress.ToString()));
				PlayerNumToClient.erase(PlayerNumToClient.find(playerNum));
				NumToProxy.erase(NumToProxy.find(playerNum));
				GameServer::Get().wait();
				BroadcastStringToAll("", (RakNet::MessageID)ID_GAME_WAIT_MESSAGE, peer);
			}
			
			break;
		case ID_CONNECTION_LOST:
			// Connection lost normally
			printf("ID_CONNECTION_LOST_NOTIFICATION\n");
			
			if (AddressToProxy.find(aPacket->systemAddress.ToString()) != AddressToProxy.end())
			{
				int playerNum = AddressToProxy[aPacket->systemAddress.ToString()]->GetPlayerID();

				delete AddressToProxy[aPacket->systemAddress.ToString()];
				AddressToProxy.erase(AddressToProxy.find(aPacket->systemAddress.ToString()));
				PlayerNumToClient.erase(PlayerNumToClient.find(playerNum));
				NumToProxy.erase(NumToProxy.find(playerNum));
				GameServer::Get().wait();
				BroadcastStringToAll("", (RakNet::MessageID)ID_GAME_WAIT_MESSAGE, peer);
			}

			break;
		case ID_INCOMPATIBLE_PROTOCOL_VERSION:
			printf("ID_INCOMPATIBLE_PROTOCOL_VERSION\n");
			break;

		case ID_CLIENT_READY_MESSAGE:
		{
			std::cout << "Connected to player" << std::endl;

			if (PlayerNumToClient.size() == 2)
			{
				GameServer::Get().begin();
				//Tell client we are waiting to start game
				BroadcastStringToAll("", (RakNet::MessageID)ID_GAME_BEGIN_MESSAGE, peer);
			}
			else
			{
				GameServer::Get().wait();
				//Tell client we are Ready to begin game
				BroadcastStringToAll("", (RakNet::MessageID)ID_GAME_WAIT_MESSAGE, peer);
			}

		}
		break;

		case ID_PLAYER_POSITION_MESSAGE:
		{
			int PlayerNum, junk;

			if (PlayerNumToClient.find(0) != PlayerNumToClient.end() && PlayerNumToClient.at(0) == aPacket->systemAddress)
			{
				PlayerNum = 0;
			}
			else if(PlayerNumToClient.find(1) != PlayerNumToClient.end() && PlayerNumToClient.at(1) == aPacket->systemAddress)
			{
				PlayerNum = 1;
			}

			RakNet::BitStream bsIn(aPacket->data, aPacket->length, false);
			bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
			bsIn.Read(junk);
			GameServer::Get().GetPlayer(PlayerNum)->Read(&bsIn);


			RakNet::BitStream bsOut;
			bsOut.Write((RakNet::MessageID)ID_PLAYER_POSITION_MESSAGE);
			bsOut.Write(PlayerNum);
			GameServer::Get().GetPlayer(PlayerNum)->Write(&bsOut);
			peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, aPacket->systemAddress, true);
		}
		break;

		case ID_GAME_QUIT_MESSAGE:
		{
			GameServer::Get().wait();
			BroadcastStringToAll("", (RakNet::MessageID)ID_GAME_WAIT_MESSAGE, peer);
		}
		break;

		case ID_INPUT_MESSAGE:
		{
			RakNet::BitStream bsIn(aPacket->data, aPacket->length, false);
			bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
			
			RakNet::BitStream bsOut;
			bsOut.Write((RakNet::MessageID)RTT_MESSAGE);

			
			HandleInputPacket(AddressToProxy[aPacket->systemAddress.ToString()], bsIn);
			WriteLastMoveTimestampIfDirty(bsOut, AddressToProxy[aPacket->systemAddress.ToString()]);

			peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, aPacket->systemAddress, false);
		}
		break;

		default:
			std::cout << "default" << std::endl;
			RakNet::RakString aString = GetStringFromPacket(aPacket);
			std::cout << aString << std::endl;

			//printf("\nUNIDENTIFIED PACKET!");
			break;
		}
	}
}

void NetworkServer::HandleInputPacket(ClientProxy* inClientProxy, RakNet::BitStream& inInputStream) 
{
	uint32_t moveCount = 0; 
	Move move; 
	inInputStream.Read(moveCount); 
	for (int i = 0; i< moveCount; i++) 
	{
		if (move.Read(inInputStream)) 
		{
			if (inClientProxy->GetUnprocessedMoveList().AddMoveIfNew(move)) 
			{ 
				inClientProxy->SetIsLastMoveTimestampDirty(true); 
			}
		}
	}
}

void NetworkServer::WriteLastMoveTimestampIfDirty(RakNet::BitStream& inOutputStream, ClientProxy* inClientProxy)
{
	bool isTimestampDirty = inClientProxy->IsLastMoveTimestampDirty(); 
	inOutputStream.Write(isTimestampDirty);
	if (isTimestampDirty) 
	{ 
		inOutputStream.Write(inClientProxy->GetUnprocessedMoveList().GetLastMoveTimestamp()); 
		inClientProxy->SetIsLastMoveTimestampDirty(false); 
	} 
}

void NetworkServer::SendPlayer(int num)
{
	RakNet::BitStream bsOut;
	bsOut.Write((RakNet::MessageID)ID_PLAYER_POSITION_MESSAGE);
	bsOut.Write(num);
	GameServer::Get().GetPlayer(num)->Write(&bsOut);
	peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
}

ClientProxy* NetworkServer::GetProxy(int playerNum)
{
	std::map<int, ClientProxy*>::iterator iter = NumToProxy.find(playerNum);
	if (iter != NumToProxy.end())
	{
		return iter->second;
	}

	return nullptr;
}

void NetworkServer::UpdateScore(std::string score)
{
	BroadcastStringToAll(score, (RakNet::MessageID)ID_SCORE_UPDATE, peer);
}
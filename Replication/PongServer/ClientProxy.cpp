#include "stdafx.h"
#include "ClientProxy.h"


ClientProxy::ClientProxy(RakNet::AddressOrGUID sysAddress, int playerId) :
	SysAddress(sysAddress),
	PlayerID(playerId)
{
}


ClientProxy::~ClientProxy()
{
}

float ClientProxy::ProcessPacket(RakNet::BitStream& bsIn)
{
	float lastTimeStamp;
	int NumMoves;
	//read number of moves written
	bsIn.Read(NumMoves);

	//loop through x amount of moves and add moves to move list
	for (int i = 0; i < NumMoves; ++i)
	{
		Move move;

		move.Read(bsIn);
		lastTimeStamp = move.GetTimestamp();
		mUnprocessedMoveList.AddMove(move);
	}

	return lastTimeStamp;
}


void ClientProxy::SetIsLastMoveTimestampDirty(bool isdrty)
{
	mIsLastMoveTimestampDirty = isdrty;
}

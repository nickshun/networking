#pragma once
#include "stdafx.h"
#include "RakPeerInterface.h"
#include "RakNetTypes.h"
#include "MessageIdentifiers.h"
#include "BitStream.h"
#include <cstdio>
#include <iostream>
#include <cassert>
#include "ClientProxy.h"

#include<string>
#include <vector>
#include <map>

class GameServer;

class NetworkServer
{
public:
	enum GameMessages
	{
		ID_WELCOME_MESSAGE_1 = ID_USER_PACKET_ENUM + 1,
		ID_SCORE_UPDATE,
		ID_GAME_WAIT_MESSAGE,
		ID_GAME_BEGIN_MESSAGE,
		ID_CLIENT_READY_MESSAGE,
		ID_STILL_CONNECTED_MESSAGE,
		ID_PLAYER_POSITION_MESSAGE,
		ID_BALL_POSITION_MESSAGE,
		ID_INPUT_MESSAGE,
		RTT_MESSAGE,
		ID_GAME_QUIT_MESSAGE
	};

	static NetworkServer& Get()
	{
		static NetworkServer sInstance;
		return sInstance;
	}

	~NetworkServer();

	bool Init();

	void Update();
	ClientProxy* GetProxy(int playerNum);

	void SendPlayer(int num);

	void UpdateScore(std::string score);

private:
	NetworkServer();

	const int SEND_DATA_COUNT = 15;
	int counter;

	RakNet::RakPeerInterface *peer;

	RakNet::AddressOrGUID ServerAddress;

	std::map<int, RakNet::AddressOrGUID> PlayerNumToClient;
	std::map<std::string, ClientProxy*> AddressToProxy;
	std::map<int, ClientProxy*> NumToProxy;

	void RunServer(RakNet::RakPeerInterface* server);

	void RunCommon(RakNet::RakPeerInterface* peer);

	unsigned char GetPacketIdentifier(RakNet::Packet *p);

	void SendStringToPeer(std::string aMessage, RakNet::MessageID messageID, RakNet::AddressOrGUID systemAddress, RakNet::RakPeerInterface* peer);

	void BroadcastStringFromSystemToAll(std::string aMessage, RakNet::MessageID messageID, RakNet::AddressOrGUID systemAddress, RakNet::RakPeerInterface* peer);

	void BroadcastStringToAll(std::string aMessage, RakNet::MessageID messageID, RakNet::RakPeerInterface* peer);

	RakNet::RakString GetStringFromPacket(RakNet::Packet* aPacket);

	void HandleInputPacket(ClientProxy* inClientProxy, RakNet::BitStream& inInputStream);

	void WriteLastMoveTimestampIfDirty(RakNet::BitStream& inOutputStream, ClientProxy* inClientProxy);

};

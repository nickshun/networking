#pragma once
#include "stdafx.h"

class NetworkServer;
class Paddle;
class Ball;

class GameServer
{
public:
	static GameServer& Get()
	{
		static GameServer sInstance;
		return sInstance;
	}


	~GameServer();

	bool Init();

	void Update();
	void UpdateInput();

	void resetMatch(int side);

	void wait();
	void begin();

	bool isGameRunning() { return isRunning; };

	Paddle* GetPlayer(int i);
	inline Ball* GetBall() { return ball; };
	inline int getGameDelay() { return gameDelay; };

	void bindBall();

	void endGame();

private:
	GameServer();
	const int SCREEN_WIDTH = 640;
	const int SCREEN_HEIGHT = 480;
	const int FRAME_TIME = (int)((1 / 60.0f) * 1000);

	Paddle* player1;
	Paddle* player2;
	Ball* ball;
	bool waitingForPlayer;
	int score[2];

	bool isRunning;
	int gameDelay = 0;
	int bindBallToPlayer;
};
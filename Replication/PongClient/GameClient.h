#pragma once
#include "stdafx.h"

class NetworkClient;
class Graphics;
class Paddle;
class Ball;
class InputManager;

class GameClient
{
public:
	static GameClient& Get()
	{
		static GameClient sInstance; 
		return sInstance;
	}

	~GameClient();

	bool Init();

	void Update();
	void Draw();

	Paddle* GetPlayer(int i);
	inline Ball* GetBall() { return ball; };

	void SetPlayerNum(int i);
	inline int GetPlayerNum() { return PlayerNum; };

	bool isGameRunning() { return isRunning; };

	void endGame();

private:
	GameClient();

	friend class InputManager;

	const int SCREEN_WIDTH = 640;
	const int SCREEN_HEIGHT = 480;
	const int FRAME_TIME = (int)((1 / 60.0f) * 1000);

	NetworkClient* TheNetworkClient;
	Graphics* graphicsSystem;

	Paddle* player1;
	Paddle* player2;
	Ball* ball;

	bool isRunning;
	int gameDelay = 0;

	bool waitingForPlayer;

	int PlayerNum;
};
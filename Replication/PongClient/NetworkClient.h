#pragma once
#include "stdafx.h"
#include "RakPeerInterface.h"
#include "RakNetTypes.h"
#include "MessageIdentifiers.h"
#include "BitStream.h"
#include <cstdio>
#include <iostream>
#include <cassert>

#include<string>
#include <vector>
#include <map>

class GameClient;

class NetworkClient
{
public:
	enum GameMessages
	{
		ID_WELCOME_MESSAGE_1 = ID_USER_PACKET_ENUM + 1,
		ID_SCORE_UPDATE,
		ID_GAME_WAIT_MESSAGE,
		ID_GAME_BEGIN_MESSAGE,
		ID_CLIENT_READY_MESSAGE,
		ID_STILL_CONNECTED_MESSAGE,
		ID_PLAYER_POSITION_MESSAGE,
		ID_BALL_POSITION_MESSAGE,
		ID_INPUT_MESSAGE,
		RTT_MESSAGE,
		ID_GAME_QUIT_MESSAGE
	};

	NetworkClient(GameClient* game);
	~NetworkClient();

	bool Init();

	void Update();

private:
	GameClient* gameRef;

	float RTT;

	RakNet::RakPeerInterface *peer;

	RakNet::AddressOrGUID ServerAddress;

	std::vector<std::string> ClientList;
	std::map<RakNet::AddressOrGUID, std::string> PlayerNumToClient;

	void RunClient(RakNet::RakPeerInterface* client);

	void RunCommon(RakNet::RakPeerInterface* peer);

	unsigned char GetPacketIdentifier(RakNet::Packet *p);

	void SendStringToPeer(std::string aMessage, RakNet::MessageID messageID, RakNet::AddressOrGUID systemAddress, RakNet::RakPeerInterface* peer);

	void BroadcastStringFromSystemToAll(std::string aMessage, RakNet::MessageID messageID, RakNet::AddressOrGUID systemAddress, RakNet::RakPeerInterface* peer);

	void BroadcastStringToAll(std::string aMessage, RakNet::MessageID messageID, RakNet::RakPeerInterface* peer);

	void SendInputPacket();

	RakNet::RakString GetStringFromPacket(RakNet::Packet* aPacket);
	int NetworkClient::GetIntFromPacket(RakNet::Packet* aPacket);

	int networkingWait;

	const int SEND_DATA = 10;
};

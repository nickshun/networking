#include "stdafx.h"
#include "GameClient.h"
#include "Graphics.h"
#include "NetworkClient.h"
#include "Paddle.h"
#include "Ball.h"
#include "InputManager.h"

GameClient::GameClient()
{
	TheNetworkClient = new NetworkClient(this);
	graphicsSystem = new Graphics();

	player1 = new Paddle(0, SCREEN_HEIGHT / 2);
	player1->setX(20 + player1->getWidth() / 2);
	player2 = new Paddle(0, SCREEN_HEIGHT / 2);
	player2->setX(SCREEN_WIDTH - 20 - player1->getWidth() / 2);
	ball = new Ball(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2);

	isRunning = false;
	waitingForPlayer = true;
	PlayerNum = 0;
}

bool GameClient::Init()
{
	if (graphicsSystem->initSDL(SCREEN_WIDTH, SCREEN_HEIGHT) && TheNetworkClient->Init())
	{
		isRunning = true;
		return true;
	}
	else
	{
		return false;
	}
}

GameClient::~GameClient()
{
	delete ball;
	delete player2;
	delete player1;
	delete graphicsSystem;
	delete TheNetworkClient;
}

Paddle* GameClient::GetPlayer(int i)
{
	if (i == 0)
	{
		return player1;
	}
	else if(i == 1)
	{
		return player2;
	}
	else
	{
		std::cout << "invalid player num: " << i << std::endl;
		return nullptr;
	}
}

void GameClient::SetPlayerNum(int i)
{
	PlayerNum = i;
}

void GameClient::Update()
{
	InputManager::Get().Update();

	//NetworkClientUpdate
	TheNetworkClient->Update();
}

void GameClient::Draw()
{
	graphicsSystem->ClearWindow();

	player1->Draw(graphicsSystem);
	player2->Draw(graphicsSystem);
	ball->Draw(graphicsSystem);

	graphicsSystem->UpdateDisplay();
}

void GameClient::endGame()
{
	isRunning = false;
}
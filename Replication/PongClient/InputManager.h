#pragma once
#include "MoveList.h"
#include "InputState.h"

class InputManager
{
public:
	static InputManager& Get()
	{
		static InputManager sInstance;
		return sInstance;
	}

	~InputManager() { delete Moves; };

	void Update();
	inline MoveList* GetMoveList() { return Moves; };

private:
	const int MOVE_TIME = 5;
	InputManager();
	bool wDown;
	bool sDown;
	MoveList* Moves;
	InputState state;
	int MoveTimer;
};
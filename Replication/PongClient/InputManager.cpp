#include "stdafx.h"
#include "SDL.h"
#include "GameClient.h"
#include "Paddle.h"
#include "InputManager.h"

InputManager::InputManager():
	wDown(false),
	sDown(false)
{
	Moves = new MoveList();
}

void InputManager::Update()
{
	SDL_Event event;
	if (SDL_PollEvent(&event))
	{
		if (event.type == SDL_QUIT)
		{
			GameClient::Get().endGame();
		}

		if (event.type == SDL_KEYDOWN)
		{
			SDL_Keycode keyPressed = event.key.keysym.sym;

			switch (keyPressed)
			{
			case SDLK_ESCAPE:
				GameClient::Get().endGame();
				break;
			case SDLK_w:
				wDown = true;
				break;
			case SDLK_s:
				sDown = true;
				break;
			}
		}

		if (event.type == SDL_KEYUP)
		{
			SDL_Keycode keyPressed = event.key.keysym.sym;

			switch (keyPressed)
			{
			case SDLK_w:
				wDown = false;
				break;
			case SDLK_s:
				sDown = false;
				break;
			}
		}
	}

	if (wDown)
	{
		if (GameClient::Get().PlayerNum == 0)
		{
			state.mDesiredUpAmount += GameClient::Get().player1->moveUp();
		}
		else
		{
			state.mDesiredUpAmount += GameClient::Get().player2->moveUp();
		}
	}

	if (sDown)
	{
		if (GameClient::Get().PlayerNum == 0)
		{
			state.mDesiredDownAmount += GameClient::Get().player1->moveDown(GameClient::Get().SCREEN_HEIGHT);
		}
		else
		{
			state.mDesiredDownAmount += GameClient::Get().player2->moveDown(GameClient::Get().SCREEN_HEIGHT);
		}
	}
	MoveTimer++;

	if (MoveTimer >= MOVE_TIME)
	{
		MoveTimer = 0;
		if (state.GetDesiredVerticalDelta() != 0)
		{
			Moves->AddMove(state, SDL_GetTicks());
		}
		state.mDesiredDownAmount = 0;
		state.mDesiredUpAmount = 0;
	}
}
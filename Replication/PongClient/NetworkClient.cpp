#include "stdafx.h"
#include "NetworkClient.h"
#include "GameClient.h"
#include "Paddle.h"
#include "Ball.h"
#include "MoveList.h"
#include "InputManager.h"
#include "SDL.h"


NetworkClient::NetworkClient(GameClient* game)
{
	gameRef = game;
	RTT = 0.0f;
}

NetworkClient::~NetworkClient()
{

}

bool NetworkClient::Init()
{
	peer = RakNet::RakPeerInterface::GetInstance();

	RunClient(peer);

	printf("My GUID is %s\n", peer->GetGuidFromSystemAddress(RakNet::UNASSIGNED_SYSTEM_ADDRESS).ToString());

	return true;
}

void NetworkClient::Update()
{
	RunCommon(peer);

	if (!gameRef->isGameRunning())
	{
		SendStringToPeer("", (RakNet::MessageID)ID_GAME_QUIT_MESSAGE, ServerAddress, peer);
	}
	else
	{
		networkingWait++;
		if (networkingWait >= SEND_DATA)
		{
			networkingWait = 0;
			SendInputPacket();
		}
	}
}

void NetworkClient::SendInputPacket() 
{ //only send if there's any input to send! 
	MoveList* moveList = InputManager::Get().GetMoveList();
	if (moveList->HasMoves()) 
	{
		RakNet::BitStream inputPacket; 
		inputPacket.Write((RakNet::MessageID)ID_INPUT_MESSAGE);
		//we only want to send the last three moves 
		int moveCount = moveList->GetMoveCount();
		int startIndex = moveCount > 3 ? moveCount - 3 - 1: 0; 
		inputPacket.Write(moveCount - startIndex);

		for(int i = startIndex; i < moveCount; ++i) 
		{
			moveList->GetMove(i).Write(inputPacket);
		}
		peer->Send(&inputPacket, HIGH_PRIORITY, RELIABLE_ORDERED, 0, ServerAddress, false);
		
		moveList->Clear();
	}
}

unsigned char NetworkClient::GetPacketIdentifier(RakNet::Packet *p)
{
	if (p == 0)
		return 255;

	if ((unsigned char)p->data[0] == ID_TIMESTAMP)
	{
		RakAssert(p->length > sizeof(RakNet::MessageID) + sizeof(RakNet::Time));
		return (unsigned char)p->data[sizeof(RakNet::MessageID) + sizeof(RakNet::Time)];
	}
	else
		return (unsigned char)p->data[0];
}

void NetworkClient::SendStringToPeer(std::string aMessage, RakNet::MessageID messageID, RakNet::AddressOrGUID systemAddress, RakNet::RakPeerInterface* peer)
{
	// Use a BitStream to write a custom user message
	// Bitstreams are easier to use than sending casted structures, and handle endian swapping automatically
	RakNet::BitStream bsOut;
	bsOut.Write(messageID);
	bsOut.Write(aMessage.c_str());

	peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, systemAddress, false);

}

void NetworkClient::BroadcastStringFromSystemToAll(std::string aMessage, RakNet::MessageID messageID, RakNet::AddressOrGUID systemAddress, RakNet::RakPeerInterface* peer)
{
	RakNet::BitStream bsOut;
	bsOut.Write(messageID);
	bsOut.Write(aMessage.c_str());
	//UNASSIGNED_SYSTEM_ADDRESS means we dont ignore anyone on the list (no filters)
	//The "true" as the last parameter means we want to broadcast to all
	peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, systemAddress, true);

}

void NetworkClient::BroadcastStringToAll(std::string aMessage, RakNet::MessageID messageID, RakNet::RakPeerInterface* peer)
{

	BroadcastStringFromSystemToAll(aMessage, messageID, RakNet::UNASSIGNED_SYSTEM_ADDRESS, peer);

}

RakNet::RakString NetworkClient::GetStringFromPacket(RakNet::Packet* aPacket)
{
	RakNet::RakString aName;
	RakNet::BitStream bsIn(aPacket->data, aPacket->length, false);
	bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
	bsIn.Read(aName);
	return aName;

}

int NetworkClient::GetIntFromPacket(RakNet::Packet* aPacket)
{
	int aNumber;
	RakNet::BitStream bsIn(aPacket->data, aPacket->length, false);
	bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
	bsIn.Read(aNumber);
	return aNumber;

}

void NetworkClient::RunClient(RakNet::RakPeerInterface* client)
{
	//Don't allow someone-else to respond and accept my connection request
	client->AllowConnectionResponseIPMigration(false);

	RakNet::SocketDescriptor socketDescriptor;
	socketDescriptor.socketFamily = AF_INET;
	//Need only one connection at a time, since we only connect to server
	client->Startup(1, &socketDescriptor, 1);
	client->SetOccasionalPing(true);

	RakNet::ConnectionAttemptResult car = client->Connect
	("localhost", 200, "Rumpelstiltskin", (int)strlen("Rumpelstiltskin"));

	//Make sure we get an "error" if we cannot connect.
	//Great for debugging
	RakAssert(car == RakNet::CONNECTION_ATTEMPT_STARTED);

	printf("\nMy IP addresses AS A CLIENT:\n");
	unsigned int i;
	for (i = 0; i < client->GetNumberOfAddresses(); i++)
	{
		printf("%i. %s\n", i + 1, client->GetLocalIP(i));
	}
}

void NetworkClient::RunCommon(RakNet::RakPeerInterface* peer)
{
	RakNet::Packet* aPacket = nullptr;

	for (aPacket = peer->Receive(); aPacket; peer->DeallocatePacket(aPacket), aPacket = peer->Receive())
	{
		// We got a packet, get the identifier with our handy function
		auto packetIdentifier = GetPacketIdentifier(aPacket);

		// Check if this is a network message packet
		switch (packetIdentifier)
		{
			// CLIENT
		case ID_ALREADY_CONNECTED:
			// Connection lost normally
			printf("ID_ALREADY_CONNECTED with guid %" PRINTF_64_BIT_MODIFIER "u\n", aPacket->guid);
			break;
		case ID_REMOTE_DISCONNECTION_NOTIFICATION: // Server telling the clients of another client disconnecting gracefully.  You can manually broadcast this in a peer to peer enviroment if you want.
			printf("ID_REMOTE_DISCONNECTION_NOTIFICATION\n");
			break;
		case ID_REMOTE_CONNECTION_LOST: // Server telling the clients of another client disconnecting forcefully.  You can manually broadcast this in a peer to peer enviroment if you want.
			printf("ID_REMOTE_CONNECTION_LOST\n");
			break;
		case ID_REMOTE_NEW_INCOMING_CONNECTION: // Server telling the clients of another client connecting.  You can manually broadcast this in a peer to peer enviroment if you want.
			printf("ID_REMOTE_NEW_INCOMING_CONNECTION\n");
			break;
		case ID_CONNECTION_BANNED: // Banned from this server
			printf("We are banned from this server.\n");
			break;
		case ID_CONNECTION_ATTEMPT_FAILED:
			printf("Connection attempt failed\n");
			break;
		case ID_NO_FREE_INCOMING_CONNECTIONS:
			// Sorry, the server is full.  I don't do anything here but
			// A real app should tell the user
			printf("ID_NO_FREE_INCOMING_CONNECTIONS\n");
			break;

		case ID_INVALID_PASSWORD:
			printf("ID_INVALID_PASSWORD\n");
			break;

		case ID_CONNECTION_LOST:
			// Couldn't deliver a reliable packet - i.e. the other system was abnormally
			// terminated
			printf("ID_CONNECTION_LOST\n");
			break;
			// COMMON
		case ID_DISCONNECTION_NOTIFICATION:
			// Connection lost normally
			printf("ID_DISCONNECTION_NOTIFICATION\n");
			break;
		case ID_INCOMPATIBLE_PROTOCOL_VERSION:
			printf("ID_INCOMPATIBLE_PROTOCOL_VERSION\n");
			break;


		case ID_WELCOME_MESSAGE_1:
		{
			//Save Server's address (GUID infact) on welcome
			ServerAddress = aPacket->guid;

			int PlayerNum;

			RakNet::BitStream bsIn(aPacket->data, aPacket->length, false);
			bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
			bsIn.Read(PlayerNum);

			gameRef->SetPlayerNum(PlayerNum);

			SendStringToPeer("", (RakNet::MessageID)ID_CLIENT_READY_MESSAGE, aPacket->guid, peer);

		}
		break;

		case ID_GAME_WAIT_MESSAGE:
		{
			std::cout << "WAITING FOR PLAYER" << std::endl;
		}
		break;

		case ID_GAME_BEGIN_MESSAGE:
		{
			std::cout << "STARTING GAME" << std::endl;
		}
		break;

		case ID_PLAYER_POSITION_MESSAGE:
		{
			int PlayerNum;

			RakNet::BitStream bsIn(aPacket->data, aPacket->length, false);
			bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
			bsIn.Read(PlayerNum);
			gameRef->GetPlayer(PlayerNum)->Read(&bsIn);
		}
		break;

		case ID_BALL_POSITION_MESSAGE:
		{
			RakNet::BitStream bsIn(aPacket->data, aPacket->length, false);
			bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
			gameRef->GetBall()->Read(&bsIn);
		}
		break;

		case RTT_MESSAGE:
		{
			float timestamp;
			bool isDirty;
			RakNet::BitStream bsIn(aPacket->data, aPacket->length, false);
			bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
			bsIn.Read(isDirty);

			if (isDirty)
			{
				bsIn.Read(timestamp);
				RTT = SDL_GetTicks() - timestamp;
			}
		}

		case ID_SCORE_UPDATE:
		{
			RakNet::RakString aString = GetStringFromPacket(aPacket);
			std::cout << aString << std::endl;
		}
		break;

		default:
			RakNet::RakString aString = GetStringFromPacket(aPacket);
			std::cout << aString << std::endl;

			//printf("\nUNIDENTIFIED PACKET!");
			break;
		}
	}
}
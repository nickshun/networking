// RaknetBasic.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "SDL.h"
#include "GameClient.h"
#include <iostream>

int main(int argc, char** argv)
{
	if (GameClient::Get().Init())
	{
		int frameTime = (int)((1 / 60.0f) * 1000);


		while (GameClient::Get().isGameRunning())
		{
			int frameStart = SDL_GetTicks();

			GameClient::Get().Update();
			GameClient::Get().Draw();

			if (frameTime - (SDL_GetTicks() - frameStart) > 0)
			{
				SDL_Delay(frameTime - (SDL_GetTicks() - frameStart));
			}
		}
	}
	else
	{
		exit(0);
	}

	return 0;
}
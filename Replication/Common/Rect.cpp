#include "stdafx.h"
#include "Rect.h"


Rect::Rect(int xLoc, int yLoc, int width, int height)
{
	x = xLoc;
	y = yLoc;
	w = width;
	h = height;
}

Rect::~Rect()
{

}


bool Rect::doesOverlap(Rect* rect)
{
	if (x >= rect->x && x <= rect->x + rect->w && y >= rect->y && y <= rect->y + rect->h)
	{
		return true;
	}
	else if (x + w >= rect->x && x + w <= rect->x + rect->w && y + h >= rect->y && y + h <= rect->y + rect->h)
	{
		return true;
	}
	else if (x >= rect->x && x <= rect->x + rect->w && y + h >= rect->y && y + h <= rect->y + rect->h)
	{
		return true;
	}
	else if (x + w >= rect->x && x + w <= rect->x + rect->w && y >= rect->y && y <= rect->y + rect->h)
	{
		return true;
	}
	else if (rect->x >= x && rect->x <= x + w && rect->y >= y && rect->y <= y + h)
	{
		return true;
	}
	else if (rect->x + rect->w >= x && rect->x + rect->w <= x + w && rect->y + rect->h >= y && rect->y + rect->h <= y + h)
	{
		return true;
	}
	else if (rect->x >= x && rect->x <= x + w && rect->y + rect->h >= y && rect->y + rect->h <= y + h)
	{
		return true;
	}
	else if (rect->x + rect->w >= x && rect->x + rect->w <= x + w && rect->y >= y && rect->y <= y + h)
	{
		return true;
	}

	return false;
}
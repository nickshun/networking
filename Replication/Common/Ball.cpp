#include "stdafx.h"
#include "Ball.h"
#include "Graphics.h"
#include "Paddle.h"

Ball::Ball(int x, int y, int w, int h)
{
	posX = x;
	posY = y;
	width = w;
	height = h;

	xSpeed = INITIAL_SPEED;
	ySpeed = INITIAL_SPEED;

	rect = new Rect(x - width / 2, y - height / 2, width, height);
}

Ball::Ball(int x, int y)
{
	posX = x;
	posY = y;
	width = 10;
	height = 10;

	xSpeed = INITIAL_SPEED;
	ySpeed = INITIAL_SPEED;

	rect = new Rect(x - width / 2, y - height / 2, width, height);
}

Ball::~Ball()
{
	delete rect;
}

void Ball::setPos(int x, int y)
{
	posX = x;
	posY = y;
}

void Ball::setX(int x)
{
	posX = x;
	rect->x = posX - width / 2;
}

void Ball::setY(int y)
{
	posY = y;
	rect->y = posY - height / 2;
}

void Ball::Draw(Graphics* graphicsSystem)
{
	graphicsSystem->DrawRect(posX - width / 2, posY - height / 2, width, height);
}

void Ball::Update()
{
	setX(posX + xSpeed);
	setY(posY + ySpeed);
}
void Ball::setSpeedMod(int mod)
{
	if (mod < 0)
	{
		xSpeed = -INITIAL_SPEED;
	}
	else
	{
		xSpeed = INITIAL_SPEED;
	}
}


int Ball::CheckCollision(Paddle* p1, Paddle* p2, int xBound, int yBound)
{
	if (rect->y <= 0)
	{
		setY(posY + (0 - rect->y));
		ySpeed *= -1;
	}
	else if (rect->y + rect->h > yBound)
	{
		setY(posY - ((rect->y + rect->h) - yBound));
		ySpeed *= -1;
	}

	if (rect->doesOverlap(p1->GetRect()))
	{
		posX = p1->getX() + p1->getWidth() / 2 + width / 2;
		xSpeed *= -1;
	}
	else if (rect->doesOverlap(p2->GetRect()))
	{
		posX = p2->getX() - p1->getWidth() / 2 - width / 2;
		xSpeed *= -1;
	}

	//temp
	if (rect->x < 0)
	{
		return -1;
	}
	else if (rect->x + rect->w > xBound)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

void Ball::Read(RakNet::BitStream *bsIn)
{
	int xLoc;
	int yLoc;


	bsIn->Read(xLoc);
	bsIn->Read(yLoc);

	setPos(xLoc, yLoc);
}

void Ball::Write(RakNet::BitStream *bsOut)
{
	bsOut->Write(posX);
	bsOut->Write(posY);
}
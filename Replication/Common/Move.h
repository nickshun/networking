#pragma once
#include "InputState.h"
#include "BitStream.h"

class Move
{
public: 
	Move();
	Move(const InputState& inInputState, float inTimestamp, float inDeltaTime);
	~Move();

	const InputState& GetInputState() const { return mInputState; };
	float GetTimestamp() const { return mTimestamp; };
	float GetDeltaTime() const { return mDeltaTime; };
	bool Write(RakNet::BitStream& inOutputStream) const; 
	bool Read(RakNet::BitStream& inInputStream);
private:
	InputState mInputState; 
	float mTimestamp; 
	float mDeltaTime;
};


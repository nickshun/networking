#include "stdafx.h"
#include "InputState.h"


InputState::InputState() : mDesiredUpAmount(0), mDesiredDownAmount(0)
{

}


InputState::~InputState()
{
}


bool InputState::Write(RakNet::BitStream& bsOut) const
{
	bsOut.Write(mDesiredUpAmount);
	bsOut.Write(mDesiredDownAmount);
	return true;
}

bool InputState::Read(RakNet::BitStream& bsIn)
{
	bsIn.Read(mDesiredUpAmount);
	bsIn.Read(mDesiredDownAmount);
	return true;
}

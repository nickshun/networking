#include "stdafx.h"
#include "MoveList.h"
#include "InputState.h"
#include "Move.h"
#include <iostream>


MoveList::MoveList():
	mLastMoveTimestamp(0.0f),
	mMoves()
{
}


MoveList::~MoveList()
{
}

const Move& MoveList::AddMove(const InputState& inInputState, float inTimestamp) 
{
	//first move has 0 delta time 
	float deltaTime = mLastMoveTimestamp >= 0.f ? inTimestamp - mLastMoveTimestamp: 0.f;
	mMoves.emplace_back(inInputState, inTimestamp, deltaTime); 
	mLastMoveTimestamp = inTimestamp; 
	return mMoves.back();
}

bool MoveList::AddMoveIfNew(const Move& inMove) 
{ 
	float timeStamp = inMove.GetTimestamp(); 
	if (timeStamp > mLastMoveTimestamp) 
	{ 
		float deltaTime = mLastMoveTimestamp >= 0.f ? timeStamp - mLastMoveTimestamp : 0.f; mLastMoveTimestamp = timeStamp;
		mMoves.emplace_back(inMove.GetInputState(), timeStamp, deltaTime); 
		return true; 
	}
	return false; 
}

void MoveList::AddMove(Move move)
{
	mMoves.push_back(move);
}

bool MoveList::HasMoves()
{
	if (mMoves.size() > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

Move &MoveList::operator[](int i)
{
	if (i < mMoves.size())
	{
		return mMoves[i];
	}
	else
	{
		//TODO fix this later
		return mMoves[0];
	}
}

Move &MoveList::GetMove(int i)
{
	if (i < mMoves.size())
	{
		return mMoves[i];
	}
	else
	{
		//TODO fix this later
		return mMoves[0];
	}
}

void MoveList::Clear()
{
	mMoves.clear();
}

float MoveList::GetLastMoveTimestamp()
{
	if (mMoves.size() > 0)
	{
		return mMoves[mMoves.size() - 1].GetTimestamp();
	}
}

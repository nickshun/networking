#include "stdafx.h"
#include "Move.h"

Move::Move()
{
}

Move::Move(const InputState& inInputState, float inTimestamp, float inDeltaTime) : 
	mInputState(inInputState), 
	mTimestamp(inTimestamp), 
	mDeltaTime(inDeltaTime)
{
}

Move::~Move()
{
}


bool Move::Write(RakNet::BitStream& bsOut) const
{
	//Write inputState
	mInputState.Write(bsOut);

	bsOut.Write(mTimestamp);
	bsOut.Write(mDeltaTime);

	return true;
}

bool Move::Read(RakNet::BitStream& bsIn)
{
	//Read inputState
	mInputState.Read(bsIn);

	bsIn.Read(mTimestamp);
	bsIn.Read(mDeltaTime);

	return true;
}

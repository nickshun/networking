#pragma once
//#include "stdafx.h"

class Rect
{
public:
	int x;
	int y;
	int w;
	int h;

	Rect(int xLoc, int yLoc, int width, int height);
	~Rect();

	bool doesOverlap(Rect* rect);

private:
};

#pragma once
#include <vector>
#include "Move.h"

class InputState;

class MoveList
{
public:
	MoveList();
	~MoveList();

	const Move& AddMove(const InputState& inInputState, float inTimestamp);
	void AddMove(Move move);
	bool AddMoveIfNew(const Move& inMove);
	bool HasMoves();
	inline int GetMoveCount() { return mMoves.size(); };

	void Clear();

	Move &operator[](int i);

	Move& GetMove(int i);

	float GetLastMoveTimestamp();

private:
	std::vector<Move> mMoves;
	float mLastMoveTimestamp;
};


#pragma once
//#include "stdafx.h"

#include "SDL.h"
#include <iostream>



class Graphics
{
private:
	//The window we'll be rendering tofb
	SDL_Window* window;
	SDL_Renderer* renderer;

	void quitSDL();

public:
	Graphics();
	~Graphics();

	bool initSDL(int width, int height);
	void UpdateDisplay();
	void ClearWindow();
	void DrawRect(int x, int y, int width, int height);
};

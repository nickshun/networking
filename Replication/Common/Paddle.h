#pragma once
//#include "stdafx.h"
#include "Rect.h"
#include "BitStream.h"
#include "MoveList.h"

class Graphics;

class Paddle
{
public:
	Paddle(int x, int y, int w, int h);
	Paddle(int x, int y);
	~Paddle();

	void setPos(int x, int y);
	void setX(int x);
	void setY(int y);

	int moveUp();
	int moveDown(int SCREEN_BOUND);

	inline int getX() { return posX; };
	inline int getY() { return posY; };
	inline int getWidth() { return width; };
	inline int getHeight() { return height; };
	inline Rect* GetRect() { return rect; };

	void Draw(Graphics* graphics);

	bool ProcessMoves(MoveList& moves);

	void Read(RakNet::BitStream *bsIn);
	void Write(RakNet::BitStream *bsOut);

private:
	const int MOVE_SPEED = 15;

	Rect* rect;
	int posX;
	int posY;
	int width;
	int height;
};

#include "stdafx.h"
#include "Graphics.h"

Graphics::Graphics()
{
	//The window we'll be rendering tofb
	window = NULL;

	renderer = NULL;
}

Graphics::~Graphics()
{
	quitSDL();
}

bool Graphics::initSDL(int width, int height)
{
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
		return false;
	}
	else
	{
		//Create window
		window = SDL_CreateWindow("SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, SDL_WINDOW_SHOWN);
		if (window == NULL)
		{
			printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
			return false;
		}
		else
		{
			renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

			ClearWindow();
		}
	}

	return true;
}

void Graphics::quitSDL()
{
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);

	printf("Quiting SDL.\n");

	/* Shutdown all subsystems */
	SDL_Quit();

	printf("Quiting....\n");
}

void Graphics::UpdateDisplay()
{
	SDL_RenderPresent(renderer);
}

void Graphics::ClearWindow()
{
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
	SDL_RenderClear(renderer);
}

void Graphics::DrawRect(int x, int y, int width, int height)
{
	SDL_Rect rect;
	rect.x = x;
	rect.y = y;
	rect.w = width;
	rect.h = height;

	SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
	SDL_RenderFillRect(renderer, &rect);
}
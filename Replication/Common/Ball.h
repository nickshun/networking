#pragma once
//#include "stdafx.h"
#include "Rect.h"
#include "BitStream.h"

class Graphics;
class Paddle;

class Ball
{
public:
	Ball(int x, int y, int w, int h);
	Ball(int x, int y);
	~Ball();

	void setPos(int x, int y);
	void setX(int x);
	void setY(int y);
	void setSpeedMod(int mod);

	inline int getX() { return posX; };
	inline int getY() { return posY; };
	inline int getWidth() { return width; };
	inline int getHeight() { return height; };
	inline Rect* GetRect() { return rect; };

	void Update();
	int CheckCollision(Paddle* p1, Paddle* p2, int xBound, int yBound);

	void Draw(Graphics* graphics);

	void Read(RakNet::BitStream *bsIn);
	void Write(RakNet::BitStream *bsOut);

private:
	const int INITIAL_SPEED = 5;

	Rect* rect;
	int posX;
	int posY;
	int width;
	int height;

	int xSpeed;
	int ySpeed;
};

#pragma once
#include "BitStream.h"
#include <iostream>

class InputState
{
public:
	InputState();
	~InputState();

	float GetDesiredVerticalDelta() const 
	{ 
		return mDesiredDownAmount - mDesiredUpAmount;
	}
	bool Write(RakNet::BitStream& inOutputStream) const;
	bool Read(RakNet::BitStream& inInputStream);

private: 
	friend class InputManager; 
	float mDesiredUpAmount, mDesiredDownAmount; 

};


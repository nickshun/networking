#include "stdafx.h"
#include "Paddle.h"
#include "Graphics.h"
#include "Rect.h"
#include <cmath>
#include <iostream>

Paddle::Paddle(int x, int y, int w, int h)
{
	posX = x;
	posY = y;
	width = w;
	height = h;
	rect = new Rect(x - width / 2, y - height / 2, width, height);
}

Paddle::Paddle(int x, int y)
{
	posX = x;
	posY = y;
	width = 20;
	height = 60;

	rect = new Rect(x - width / 2, y - height / 2, width, height);
}

Paddle::~Paddle()
{
	delete rect;
}

void Paddle::setPos(int x, int y)
{
	posX = x;
	posY = y;
	rect->x = posX - width / 2;
	rect->y = posY - height / 2;
}

void Paddle::setX(int x)
{
	posX = x;
	rect->x = posX - width / 2;
}

void Paddle::setY(int y)
{
	posY = y;
	rect->y = posY - height / 2;
}

bool Paddle::ProcessMoves(MoveList& moves)
{
	if (moves.HasMoves())
	{
		for (int i = 0; i < moves.GetMoveCount(); ++i)
		{
			posY += moves[i].GetInputState().GetDesiredVerticalDelta();
			setY(posY);
		}

		moves.Clear();
		return true;
	}

	return false;
}

int Paddle::moveUp()
{
	int diff = 0;

	if (rect->y - MOVE_SPEED < 0)
	{
		setY(height / 2);
		diff = posY - height / 2;
	}
	else
	{
		diff = MOVE_SPEED;
		setY(posY - MOVE_SPEED);
	}

	return abs(diff);
}

int Paddle::moveDown(int SCREEN_BOUND)
{
	int diff = 0;

	if (rect->y + rect->h + MOVE_SPEED > SCREEN_BOUND)
	{
		diff = (SCREEN_BOUND - height / 2) - posY;
		setY(SCREEN_BOUND - height / 2);
	}
	else
	{
		diff = MOVE_SPEED;
		setY(posY + MOVE_SPEED);
	}
	return abs(diff);
}

void Paddle::Draw(Graphics* graphicsSystem)
{
	graphicsSystem->DrawRect(posX - width / 2, posY - height / 2, width, height);
}

void Paddle::Read(RakNet::BitStream *bsIn)
{
	int xLoc;
	int yLoc;


	bsIn->Read(xLoc);
	bsIn->Read(yLoc);

	setPos(xLoc, yLoc);
}

void Paddle::Write(RakNet::BitStream *bsOut)
{
	bsOut->Write(posX);
	bsOut->Write(posY);
}